let rootRef = firebase.database().ref();
let database = rootRef.database;

function insertUser(User) {
    return new Promise((resolve, reject) => {

        const newUser = JSON.stringify(User);
        let response;
        let keys = 0;
        let isDuplicated = Boolean; // isDuplicated, es una bandera booleana usada para indicar si
                                    //  el usuario a insertar ya existe.

        $.ajax({
            url: `${config.databaseURL}/test1/.json?print=pretty`,
            type: "GET",
            dataType: "JSON",
            success: function (res) {
               response = res;
               keys = Object.keys(response);

               for (let i = 0; i < keys.length; i++) {
                   let k = keys[i];
                   if (User.correo === response[k].correo) {
                       isDuplicated = true
                       reject({
                           message: "El usuario que intentas registrar ya existe",
                           hasError: "true"
                       });
                   }
               }

               if (!isDuplicated) {
                    $.ajax({
                        url: `${config.databaseURL}/test1/.json`,
                        type: "POST",
                        data: newUser,
                        success: function (res) {
                            resolve({
                                message: "exito al insertar usuario",
                                hasError: false
                            });
                            window.location.reload = false;
                        },
                        error: function (error) {
                            reject({
                                message: "Ocurrio un error inesperado al insertar",
                                hasError: true
                            });
                            window.location.reload = false;
                        }
                    });
               }
            },
            error: function (error) {
                reject({
                    message: "Error al obtener informacion", 
                    hasError: true
                });
            }
        });

    });
}

function MostrarUsuarios(){
    var ref = firebase.database().ref("test1");

    ref.orderByChild("correo").on("child_added", function(snapshot){
        var d = snapshot.val();
    });

}